futex_lock_pi
=> 0xc0050f30 <+0>:	push	{r4, r5, r6, r7, r8, r9, r10, r11, lr}
   0xc0050f34 <+4>:	sub	sp, sp, #132	; 0x84
   0xc0050f38 <+8>:	mov	r9, r0
   0xc0050f3c <+12>:	mov	r10, r1
   0xc0050f40 <+16>:	mov	r7, r2
   0xc0050f44 <+20>:	add	r0, sp, #72	; 0x48
   0xc0050f48 <+24>:	mov	r1, #0
   0xc0050f4c <+28>:	mov	r2, #56	; 0x38
   0xc0050f50 <+32>:	str	r3, [sp, #12]
   0xc0050f54 <+36>:	bl	0xc01cc6c0 <memset>
   0xc0050f58 <+40>:	mvn	r3, #0
   0xc0050f5c <+44>:	str	r3, [sp, #124]	; 0x7c
   0xc0050f60 <+48>:	mov	r3, sp
   0xc0050f64 <+52>:	bic	r4, r3, #8128	; 0x1fc0
   0xc0050f68 <+56>:	bic	r3, r4, #63	; 0x3f
   0xc0050f6c <+60>:	ldr	r3, [r3, #12]
   0xc0050f70 <+64>:	ldr	r3, [r3, #948]	; 0x3b4
   0xc0050f74 <+68>:	cmp	r3, #0
   0xc0050f78 <+72>:	beq	0xc0050fc0 <futex_lock_pi+144>
   0xc0050f7c <+76>:	cmp	r7, #0
   0xc0050f80 <+80>:	streq	r7, [sp, #8]
   0xc0050f84 <+84>:	beq	0xc0050fd4 <futex_lock_pi+164>
   0xc0050f88 <+88>:	mov	r1, #0
   0xc0050f8c <+92>:	bic	r5, r4, #63	; 0x3f
   0xc0050f90 <+96>:	mov	r2, r1
   0xc0050f94 <+100>:	add	r0, sp, #16
   0xc0050f98 <+104>:	bl	0xc0038360 <hrtimer_init>
   0xc0050f9c <+108>:	add	r0, sp, #16
   0xc0050fa0 <+112>:	ldr	r1, [r5, #12]
   0xc0050fa4 <+116>:	bl	0xc003891c <hrtimer_init_sleeper>
   0xc0050fa8 <+120>:	ldrd	r2, [r7]
   0xc0050fac <+124>:	strd	r2, [sp, #32]
   0xc0050fb0 <+128>:	strd	r2, [sp, #40]	; 0x28
   0xc0050fb4 <+132>:	add	r3, sp, #16
   0xc0050fb8 <+136>:	str	r3, [sp, #8]
   0xc0050fbc <+140>:	b	0xc0050fd4 <futex_lock_pi+164>
   0xc0050fc0 <+144>:	bl	0xc004fabc <refill_pi_state_cache>
   0xc0050fc4 <+148>:	cmp	r0, #0
   0xc0050fc8 <+152>:	beq	0xc0050f7c <futex_lock_pi+76>
   0xc0050fcc <+156>:	mvn	r0, #11
   0xc0050fd0 <+160>:	b	0xc0051208 <futex_lock_pi+728>
   0xc0050fd4 <+164>:	and	r10, r10, #1
   0xc0050fd8 <+168>:	add	r6, sp, #100	; 0x64
   0xc0050fdc <+172>:	mov	r0, r9
   0xc0050fe0 <+176>:	mov	r1, r10
   0xc0050fe4 <+180>:	mov	r3, #1
   0xc0050fe8 <+184>:	mov	r2, r6
   0xc0050fec <+188>:	bl	0xc004f374 <get_futex_key>
   0xc0050ff0 <+192>:	subs	r5, r0, #0
   0xc0050ff4 <+196>:	bne	0xc00511e4 <futex_lock_pi+692>
   0xc0050ff8 <+200>:	mov	r0, r6
   0xc0050ffc <+204>:	bl	0xc004f1f0 <hash_futex>
   0xc0051000 <+208>:	str	r0, [sp, #96]	; 0x60
   0xc0051004 <+212>:	mov	r7, r0
   0xc0051008 <+216>:	mov	r0, #1
   0xc005100c <+220>:	bl	0xc003e168 <add_preempt_count>
   0xc0051010 <+224>:	bic	r8, r4, #63	; 0x3f
   0xc0051014 <+228>:	mov	r0, r9
   0xc0051018 <+232>:	mov	r1, r7
   0xc005101c <+236>:	mov	r2, r6
   0xc0051020 <+240>:	ldr	r3, [r8, #12]
   0xc0051024 <+244>:	str	r3, [sp]
   0xc0051028 <+248>:	mov	r3, #0
   0xc005102c <+252>:	str	r3, [sp, #4]
   0xc0051030 <+256>:	add	r3, sp, #112	; 0x70
   0xc0051034 <+260>:	bl	0xc004f97c <futex_lock_pi_atomic>
   0xc0051038 <+264>:	subs	r11, r0, #0
   0xc005103c <+268>:	beq	0xc0051088 <futex_lock_pi+344>
   0xc0051040 <+272>:	cmn	r11, #11
   0xc0051044 <+276>:	beq	0xc0051060 <futex_lock_pi+304>
   0xc0051048 <+280>:	cmp	r11, #1
   0xc005104c <+284>:	beq	0xc0051190 <futex_lock_pi+608>
   0xc0051050 <+288>:	cmn	r11, #14
   0xc0051054 <+292>:	beq	0xc00511b0 <futex_lock_pi+640>
   0xc0051058 <+296>:	mov	r5, r11
   0xc005105c <+300>:	b	0xc0051190 <futex_lock_pi+608>
   0xc0051060 <+304>:	mov	r0, #1
   0xc0051064 <+308>:	bl	0xc003e254 <sub_preempt_count>
   0xc0051068 <+312>:	ldr	r3, [r8]
   0xc005106c <+316>:	tst	r3, #2
   0xc0051070 <+320>:	beq	0xc0051078 <futex_lock_pi+328>
   0xc0051074 <+324>:	bl	0xc035dc98 <preempt_schedule>
   0xc0051078 <+328>:	mov	r0, r6
   0xc005107c <+332>:	bl	0xc004fbe8 <drop_futex_key_refs>
   0xc0051080 <+336>:	bl	0xc035e758 <_cond_resched>
   0xc0051084 <+340>:	b	0xc0050fd8 <futex_lock_pi+168>
   0xc0051088 <+344>:	ldr	r3, [r8, #12]
   0xc005108c <+348>:	add	r0, sp, #72	; 0x48
   0xc0051090 <+352>:	mov	r1, r7
   0xc0051094 <+356>:	ldr	r3, [r3, #32]
   0xc0051098 <+360>:	cmp	r3, #100	; 0x64
   0xc005109c <+364>:	movge	r3, #100	; 0x64
   0xc00510a0 <+368>:	str	r3, [sp, #72]	; 0x48
   0xc00510a4 <+372>:	add	r3, sp, #76	; 0x4c
   0xc00510a8 <+376>:	str	r3, [sp, #76]	; 0x4c
   0xc00510ac <+380>:	str	r3, [sp, #80]	; 0x50
   0xc00510b0 <+384>:	add	r3, sp, #84	; 0x54
   0xc00510b4 <+388>:	str	r3, [sp, #84]	; 0x54
   0xc00510b8 <+392>:	str	r3, [sp, #88]	; 0x58
   0xc00510bc <+396>:	bl	0xc01cffc8 <plist_add>
   0xc00510c0 <+400>:	ldr	r3, [r8, #12]
   0xc00510c4 <+404>:	str	r3, [sp, #92]	; 0x5c
   0xc00510c8 <+408>:	mov	r0, #1
   0xc00510cc <+412>:	bl	0xc003e254 <sub_preempt_count>
   0xc00510d0 <+416>:	ldr	r3, [r8]
   0xc00510d4 <+420>:	tst	r3, #2
   0xc00510d8 <+424>:	beq	0xc00510e0 <futex_lock_pi+432>
   0xc00510dc <+428>:	bl	0xc035dc98 <preempt_schedule>
   0xc00510e0 <+432>:	ldr	r3, [sp, #112]	; 0x70
   0xc00510e4 <+436>:	cmp	r3, #0
   0xc00510e8 <+440>:	bne	0xc00510f8 <futex_lock_pi+456>
   0xc00510ec <+444>:	ldr	r0, [pc, #284]	; 0xc0051210 <futex_lock_pi+736>
   0xc00510f0 <+448>:	movw	r1, #2043	; 0x7fb
   0xc00510f4 <+452>:	bl	0xc0018f48 <warn_slowpath_null>
   0xc00510f8 <+456>:	ldr	r3, [sp, #12]
   0xc00510fc <+460>:	ldr	r0, [sp, #112]	; 0x70
   0xc0051100 <+464>:	cmp	r3, #0
   0xc0051104 <+468>:	add	r0, r0, #8
   0xc0051108 <+472>:	bne	0xc0051120 <futex_lock_pi+496>
   0xc005110c <+476>:	ldr	r1, [sp, #8]
   0xc0051110 <+480>:	mov	r2, #1
   0xc0051114 <+484>:	bl	0xc0052b40 <rt_mutex_timed_lock>
   0xc0051118 <+488>:	mov	r11, r0
   0xc005111c <+492>:	b	0xc0051130 <futex_lock_pi+512>
   0xc0051120 <+496>:	bl	0xc035ed40 <rt_mutex_trylock>
   0xc0051124 <+500>:	cmp	r0, #0
   0xc0051128 <+504>:	movne	r11, #0
   0xc005112c <+508>:	mvneq	r11, #10
   0xc0051130 <+512>:	mov	r0, #1
   0xc0051134 <+516>:	bl	0xc003e168 <add_preempt_count>
   0xc0051138 <+520>:	clz	r2, r11
   0xc005113c <+524>:	mov	r0, r9
   0xc0051140 <+528>:	add	r1, sp, #72	; 0x48
   0xc0051144 <+532>:	lsr	r2, r2, #5
   0xc0051148 <+536>:	bl	0xc00502cc <fixup_owner>
   0xc005114c <+540>:	cmp	r0, #0
   0xc0051150 <+544>:	andne	r11, r0, r0, asr #31
   0xc0051154 <+548>:	cmp	r11, #0
   0xc0051158 <+552>:	beq	0xc0051180 <futex_lock_pi+592>
   0xc005115c <+556>:	ldr	r0, [sp, #112]	; 0x70
   0xc0051160 <+560>:	bic	r4, r4, #63	; 0x3f
   0xc0051164 <+564>:	ldr	r1, [r4, #12]
   0xc0051168 <+568>:	ldr	r2, [r0, #16]
   0xc005116c <+572>:	bic	r2, r2, #1
   0xc0051170 <+576>:	cmp	r1, r2
   0xc0051174 <+580>:	bne	0xc0051180 <futex_lock_pi+592>
   0xc0051178 <+584>:	add	r0, r0, #8
   0xc005117c <+588>:	bl	0xc035edbc <rt_mutex_unlock>
   0xc0051180 <+592>:	add	r0, sp, #72	; 0x48
   0xc0051184 <+596>:	mov	r5, r11
   0xc0051188 <+600>:	bl	0xc0050ba4 <unqueue_me_pi>
   0xc005118c <+604>:	b	0xc00511dc <futex_lock_pi+684>
   0xc0051190 <+608>:	mov	r0, #1
   0xc0051194 <+612>:	bl	0xc003e254 <sub_preempt_count>
   0xc0051198 <+616>:	bic	r4, r4, #63	; 0x3f
   0xc005119c <+620>:	ldr	r3, [r4]
   0xc00511a0 <+624>:	tst	r3, #2
   0xc00511a4 <+628>:	beq	0xc00511dc <futex_lock_pi+684>
   0xc00511a8 <+632>:	bl	0xc035dc98 <preempt_schedule>
   0xc00511ac <+636>:	b	0xc00511dc <futex_lock_pi+684>
   0xc00511b0 <+640>:	mov	r0, #1
   0xc00511b4 <+644>:	bl	0xc003e254 <sub_preempt_count>
   0xc00511b8 <+648>:	ldr	r3, [r8]
   0xc00511bc <+652>:	tst	r3, #2
   0xc00511c0 <+656>:	beq	0xc00511c8 <futex_lock_pi+664>
   0xc00511c4 <+660>:	bl	0xc035dc98 <preempt_schedule>
   0xc00511c8 <+664>:	mov	r0, r9
   0xc00511cc <+668>:	bl	0xc004f2dc <fault_in_user_writeable>
   0xc00511d0 <+672>:	cmp	r0, #0
   0xc00511d4 <+676>:	beq	0xc00511f4 <futex_lock_pi+708>
   0xc00511d8 <+680>:	mov	r5, r0
   0xc00511dc <+684>:	mov	r0, r6
   0xc00511e0 <+688>:	bl	0xc004fbe8 <drop_futex_key_refs>
   0xc00511e4 <+692>:	cmn	r5, #4
   0xc00511e8 <+696>:	movne	r0, r5
   0xc00511ec <+700>:	mvneq	r0, #512	; 0x200
   0xc00511f0 <+704>:	b	0xc0051208 <futex_lock_pi+728>
   0xc00511f4 <+708>:	cmp	r10, #0
   0xc00511f8 <+712>:	beq	0xc0050ff8 <futex_lock_pi+200>
   0xc00511fc <+716>:	mov	r0, r6
   0xc0051200 <+720>:	bl	0xc004fbe8 <drop_futex_key_refs>
   0xc0051204 <+724>:	b	0xc0050fd8 <futex_lock_pi+168>
   0xc0051208 <+728>:	add	sp, sp, #132	; 0x84
   0xc005120c <+732>:	pop	{r4, r5, r6, r7, r8, r9, r10, r11, pc}
   0xc0051210 <+736>:	eorsgt	r7, lr, r10, lsl #20
End of assembler dump.