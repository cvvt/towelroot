Dump of assembler code for function __sys_sendmmsg:
   0xc025b15c <+0>:	cmp	r2, #1024	; 0x400
   0xc025b160 <+4>:	push	{r4, r5, r6, r7, r8, r9, r10, lr}
   0xc025b164 <+8>:	sub	sp, sp, #176	; 0xb0
   0xc025b168 <+12>:	movcc	r8, r2
   0xc025b16c <+16>:	mov	r5, r1
   0xc025b170 <+20>:	add	r2, sp, #8
   0xc025b174 <+24>:	add	r1, sp, #12
   0xc025b178 <+28>:	movcs	r8, #1024	; 0x400
   0xc025b17c <+32>:	mov	r9, r3
   0xc025b180 <+36>:	bl	0xc0259304 <sockfd_lookup_light>
   0xc025b184 <+40>:	subs	r6, r0, #0
   0xc025b188 <+44>:	beq	0xc025b258 <__sys_sendmmsg+252>
   0xc025b18c <+48>:	mvn	r3, #0
   0xc025b190 <+52>:	str	r3, [sp, #172]	; 0xac
=> 0xc025b194 <+56>:	mov	r3, sp
   0xc025b198 <+60>:	bic	r7, r3, #8128	; 0x1fc0
   0xc025b19c <+64>:	bic	r10, r7, #63	; 0x3f
   0xc025b1a0 <+68>:	mov	r4, #0
   0xc025b1a4 <+72>:	str	r4, [sp, #12]
   0xc025b1a8 <+76>:	cmp	r4, r8
   0xc025b1ac <+80>:	beq	0xc025b20c <__sys_sendmmsg+176>
   0xc025b1b0 <+84>:	bic	r3, r7, #63	; 0x3f
   0xc025b1b4 <+88>:	ldr	r3, [r3, #8]
   0xc025b1b8 <+92>:	adds	r2, r5, #28
   0xc025b1bc <+96>:	sbcscc	r2, r2, r3
   0xc025b1c0 <+100>:	movcc	r3, #0
   0xc025b1c4 <+104>:	cmp	r3, #0
   0xc025b1c8 <+108>:	bne	0xc025b1f0 <__sys_sendmmsg+148>
   0xc025b1cc <+112>:	add	r3, sp, #44	; 0x2c
   0xc025b1d0 <+116>:	add	r2, sp, #16
   0xc025b1d4 <+120>:	str	r3, [sp]
   0xc025b1d8 <+124>:	mov	r0, r6
   0xc025b1dc <+128>:	mov	r1, r5
   0xc025b1e0 <+132>:	mov	r3, r9
   0xc025b1e4 <+136>:	bl	0xc0259d70 <___sys_sendmsg>
   0xc025b1e8 <+140>:	mov	r2, r0
   0xc025b1ec <+144>:	b	0xc025b200 <__sys_sendmmsg+164>
   0xc025b1f0 <+148>:	add	r0, sp, #16
   0xc025b1f4 <+152>:	mov	r1, #28
   0xc025b1f8 <+156>:	bl	0xc01cc7a0 <__memzero>
   0xc025b1fc <+160>:	mvn	r2, #13
   0xc025b200 <+164>:	cmp	r2, #0
   0xc025b204 <+168>:	str	r2, [sp, #12]
   0xc025b208 <+172>:	bge	0xc025b224 <__sys_sendmmsg+200>
   0xc025b20c <+176>:	ldr	r3, [sp, #8]
   0xc025b210 <+180>:	ldr	r0, [r6, #16]
   0xc025b214 <+184>:	cmp	r3, #0
   0xc025b218 <+188>:	beq	0xc025b24c <__sys_sendmmsg+240>
   0xc025b21c <+192>:	bl	0xc00b31a4 <fput>
   0xc025b220 <+196>:	b	0xc025b24c <__sys_sendmmsg+240>
   0xc025b224 <+200>:	ldr	r1, [r10, #8]
   0xc025b228 <+204>:	add	r0, r5, #28
   0xc025b22c <+208>:	sub	r1, r1, #1
   0xc025b230 <+212>:	bl	0xc01cc854 <__put_user_4>
   0xc025b234 <+216>:	cmp	r0, #0
   0xc025b238 <+220>:	str	r0, [sp, #12]
   0xc025b23c <+224>:	add	r5, r5, #32
   0xc025b240 <+228>:	bne	0xc025b20c <__sys_sendmmsg+176>
   0xc025b244 <+232>:	add	r4, r4, #1
   0xc025b248 <+236>:	b	0xc025b1a8 <__sys_sendmmsg+76>
   0xc025b24c <+240>:	cmp	r4, #0
   0xc025b250 <+244>:	movne	r0, r4
   0xc025b254 <+248>:	bne	0xc025b25c <__sys_sendmmsg+256>
   0xc025b258 <+252>:	ldr	r0, [sp, #12]
   0xc025b25c <+256>:	add	sp, sp, #176	; 0xb0
   0xc025b260 <+260>:	pop	{r4, r5, r6, r7, r8, r9, r10, pc}
End of assembler dump.