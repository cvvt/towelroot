Dump of assembler code for function ___sys_sendmsg:
=> 0xc0259d70 <+0>:	push	{r4, r5, r6, r7, r8, r9, r10, r11, lr}
   0xc0259d74 <+4>:	sub	sp, sp, #236	; 0xec
   0xc0259d78 <+8>:	mov	r6, r0
   0xc0259d7c <+12>:	mov	r11, r2
   0xc0259d80 <+16>:	mov	r0, r2
   0xc0259d84 <+20>:	mov	r2, #28
   0xc0259d88 <+24>:	mov	r10, r3
   0xc0259d8c <+28>:	ldr	r8, [sp, #272]	; 0x110
   0xc0259d90 <+32>:	bl	0xc01cb2b0 <__copy_from_user>
   0xc0259d94 <+36>:	cmp	r0, #0
   0xc0259d98 <+40>:	mvnne	r0, #13
   0xc0259d9c <+44>:	bne	0xc0259f98 <___sys_sendmsg+552>
   0xc0259da0 <+48>:	ldr	r3, [r11, #12]
   0xc0259da4 <+52>:	cmp	r3, #1024	; 0x400
   0xc0259da8 <+56>:	mvnhi	r0, #89	; 0x59
   0xc0259dac <+60>:	bhi	0xc0259f98 <___sys_sendmsg+552>
   0xc0259db0 <+64>:	cmp	r3, #8
   0xc0259db4 <+68>:	lsl	r2, r3, #3
   0xc0259db8 <+72>:	str	r2, [sp, #4]
   0xc0259dbc <+76>:	bls	0xc0259ddc <___sys_sendmsg+108>
   0xc0259dc0 <+80>:	mov	r1, r2
   0xc0259dc4 <+84>:	ldr	r0, [r6, #20]
   0xc0259dc8 <+88>:	mov	r2, #208	; 0xd0
   0xc0259dcc <+92>:	bl	0xc025d10c <sock_kmalloc>
   0xc0259dd0 <+96>:	subs	r9, r0, #0
   0xc0259dd4 <+100>:	bne	0xc0259de0 <___sys_sendmsg+112>
   0xc0259dd8 <+104>:	b	0xc0259f8c <___sys_sendmsg+540>
   0xc0259ddc <+108>:	add	r9, sp, #40	; 0x28
   0xc0259de0 <+112>:	mov	r0, r11
   0xc0259de4 <+116>:	mov	r1, r9
   0xc0259de8 <+120>:	add	r2, sp, #104	; 0x68
   0xc0259dec <+124>:	mov	r3, #0
   0xc0259df0 <+128>:	bl	0xc0263510 <verify_iovec>
   0xc0259df4 <+132>:	subs	r4, r0, #0
   0xc0259df8 <+136>:	blt	0xc0259f6c <___sys_sendmsg+508>
   0xc0259dfc <+140>:	ldr	r5, [r11, #20]
   0xc0259e00 <+144>:	cmp	r5, #0
   0xc0259e04 <+148>:	bge	0xc0259e10 <___sys_sendmsg+160>
   0xc0259e08 <+152>:	mvn	r4, #104	; 0x68
   0xc0259e0c <+156>:	b	0xc0259f6c <___sys_sendmsg+508>
   0xc0259e10 <+160>:	beq	0xc0259e98 <___sys_sendmsg+296>
   0xc0259e14 <+164>:	cmp	r5, #32
   0xc0259e18 <+168>:	addls	r7, sp, #8
   0xc0259e1c <+172>:	bls	0xc0259e38 <___sys_sendmsg+200>
   0xc0259e20 <+176>:	ldr	r0, [r6, #20]
   0xc0259e24 <+180>:	mov	r1, r5
   0xc0259e28 <+184>:	mov	r2, #208	; 0xd0
   0xc0259e2c <+188>:	bl	0xc025d10c <sock_kmalloc>
   0xc0259e30 <+192>:	subs	r7, r0, #0
   0xc0259e34 <+196>:	beq	0xc0259e08 <___sys_sendmsg+152>
   0xc0259e38 <+200>:	mov	r3, sp
   0xc0259e3c <+204>:	bic	r3, r3, #8128	; 0x1fc0
   0xc0259e40 <+208>:	bic	r3, r3, #63	; 0x3f
   0xc0259e44 <+212>:	ldr	r1, [r11, #16]
   0xc0259e48 <+216>:	ldr	r3, [r3, #8]
   0xc0259e4c <+220>:	adds	r2, r1, r5
   0xc0259e50 <+224>:	sbcscc	r2, r2, r3
   0xc0259e54 <+228>:	movcc	r3, #0
   0xc0259e58 <+232>:	cmp	r3, #0
   0xc0259e5c <+236>:	bne	0xc0259e78 <___sys_sendmsg+264>
   0xc0259e60 <+240>:	mov	r0, r7
   0xc0259e64 <+244>:	mov	r2, r5
   0xc0259e68 <+248>:	bl	0xc01cb2b0 <__copy_from_user>
   0xc0259e6c <+252>:	cmp	r0, #0
   0xc0259e70 <+256>:	bne	0xc0259f4c <___sys_sendmsg+476>
   0xc0259e74 <+260>:	b	0xc0259e90 <___sys_sendmsg+288>
   0xc0259e78 <+264>:	cmp	r5, #0
   0xc0259e7c <+268>:	beq	0xc0259e90 <___sys_sendmsg+288>
   0xc0259e80 <+272>:	mov	r0, r7
   0xc0259e84 <+276>:	mov	r1, r5
   0xc0259e88 <+280>:	bl	0xc01cc7a0 <__memzero>
   0xc0259e8c <+284>:	b	0xc0259f4c <___sys_sendmsg+476>
   0xc0259e90 <+288>:	str	r7, [r11, #16]
   0xc0259e94 <+292>:	b	0xc0259e9c <___sys_sendmsg+300>
   0xc0259e98 <+296>:	add	r7, sp, #8
   0xc0259e9c <+300>:	str	r10, [r11, #24]
   0xc0259ea0 <+304>:	ldr	r3, [r6, #16]
   0xc0259ea4 <+308>:	ldr	r3, [r3, #24]
   0xc0259ea8 <+312>:	tst	r3, #2048	; 0x800
   0xc0259eac <+316>:	orrne	r10, r10, #64	; 0x40
   0xc0259eb0 <+320>:	strne	r10, [r11, #24]
   0xc0259eb4 <+324>:	cmp	r8, #0
   0xc0259eb8 <+328>:	beq	0xc0259f00 <___sys_sendmsg+400>
   0xc0259ebc <+332>:	ldr	r1, [r11]
   0xc0259ec0 <+336>:	cmp	r1, #0
   0xc0259ec4 <+340>:	beq	0xc0259f00 <___sys_sendmsg+400>
   0xc0259ec8 <+344>:	ldr	r2, [r8, #128]	; 0x80
   0xc0259ecc <+348>:	ldr	r3, [r11, #4]
   0xc0259ed0 <+352>:	cmp	r2, r3
   0xc0259ed4 <+356>:	bne	0xc0259f00 <___sys_sendmsg+400>
   0xc0259ed8 <+360>:	mov	r0, r8
   0xc0259edc <+364>:	bl	0xc01d495c <memcmp>
   0xc0259ee0 <+368>:	cmp	r0, #0
   0xc0259ee4 <+372>:	bne	0xc0259f00 <___sys_sendmsg+400>
   0xc0259ee8 <+376>:	mov	r2, r4
   0xc0259eec <+380>:	mov	r0, r6
   0xc0259ef0 <+384>:	mov	r1, r11
   0xc0259ef4 <+388>:	bl	0xc02590e4 <sock_sendmsg_nosec>
   0xc0259ef8 <+392>:	mov	r4, r0
   0xc0259efc <+396>:	b	0xc0259f50 <___sys_sendmsg+480>
   0xc0259f00 <+400>:	mov	r2, r4
   0xc0259f04 <+404>:	mov	r0, r6
   0xc0259f08 <+408>:	mov	r1, r11
   0xc0259f0c <+412>:	bl	0xc0259c98 <sock_sendmsg>
   0xc0259f10 <+416>:	cmp	r8, #0
   0xc0259f14 <+420>:	mvn	r3, r0
   0xc0259f18 <+424>:	mov	r4, r0
   0xc0259f1c <+428>:	lsr	r3, r3, #31
   0xc0259f20 <+432>:	moveq	r3, #0
   0xc0259f24 <+436>:	cmp	r3, #0
   0xc0259f28 <+440>:	beq	0xc0259f50 <___sys_sendmsg+480>
   0xc0259f2c <+444>:	ldr	r2, [r11, #4]
   0xc0259f30 <+448>:	str	r2, [r8, #128]	; 0x80
   0xc0259f34 <+452>:	ldr	r1, [r11]
   0xc0259f38 <+456>:	cmp	r1, #0
   0xc0259f3c <+460>:	beq	0xc0259f50 <___sys_sendmsg+480>
   0xc0259f40 <+464>:	mov	r0, r8
   0xc0259f44 <+468>:	bl	0xc01cc040 <memcpy>
   0xc0259f48 <+472>:	b	0xc0259f50 <___sys_sendmsg+480>
   0xc0259f4c <+476>:	mvn	r4, #13
   0xc0259f50 <+480>:	add	r3, sp, #8
   0xc0259f54 <+484>:	cmp	r7, r3
   0xc0259f58 <+488>:	beq	0xc0259f6c <___sys_sendmsg+508>
   0xc0259f5c <+492>:	ldr	r0, [r6, #20]
   0xc0259f60 <+496>:	mov	r1, r7
   0xc0259f64 <+500>:	mov	r2, r5
   0xc0259f68 <+504>:	bl	0xc025d188 <sock_kfree_s>
   0xc0259f6c <+508>:	add	r3, sp, #40	; 0x28
   0xc0259f70 <+512>:	cmp	r9, r3
   0xc0259f74 <+516>:	beq	0xc0259f94 <___sys_sendmsg+548>
   0xc0259f78 <+520>:	ldr	r0, [r6, #20]
   0xc0259f7c <+524>:	mov	r1, r9
   0xc0259f80 <+528>:	ldr	r2, [sp, #4]
   0xc0259f84 <+532>:	bl	0xc025d188 <sock_kfree_s>
   0xc0259f88 <+536>:	b	0xc0259f94 <___sys_sendmsg+548>
   0xc0259f8c <+540>:	mvn	r0, #11
   0xc0259f90 <+544>:	b	0xc0259f98 <___sys_sendmsg+552>
   0xc0259f94 <+548>:	mov	r0, r4
   0xc0259f98 <+552>:	add	sp, sp, #236	; 0xec
   0xc0259f9c <+556>:	pop	{r4, r5, r6, r7, r8, r9, r10, r11, pc}