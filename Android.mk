
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := debugexploit
LOCAL_SRC_FILES := socket.c main.c
LOCAL_LDFLAGS   += -llog -fPIE -pie
LOCAL_CFLAGS    += -DDEBUG -DPORT=12345
LOCAL_CFLAGS    += -fno-stack-protector -O0 -fPIE
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_CFLAGS    += -fno-stack-protector -O0 
LOCAL_MODULE    := exploit
LOCAL_CFLAGS	+= -DPORT=12345
LOCAL_SRC_FILES := socket.c main.c

include $(BUILD_SHARED_LIBRARY)


